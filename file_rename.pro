QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

RC_ICONS = main.ico

CONFIG += c++11

TARGET = filerename

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += \
    include/common \
    include/main \
    include/module \
    include/tool

SOURCES += \
    source/common/FRCommon.cpp \
    source/common/FRFileInfo.cpp \
    source/common/FRLineEdit.cpp \
    source/common/FRRadioButton.cpp \
    source/module/FRInitMod.cpp \
    source/module/FRAddMod.cpp \
    source/module/FRRepMod.cpp \
    source/module/FRNumMod.cpp \
    source/module/FRLetMod.cpp \
    source/module/FRModule.cpp \
    source/module/FRModuleDef.cpp \
    source/module/FRModuleFactory.cpp \
    source/tool/FRDealFile.cpp \
    source/tool/FRHistory.cpp \
    source/tool/FRThreadPool.cpp \
    source/tool/FRAdjustThread.cpp \
    source/tool/FRDealPart.cpp \
    source/main/FileRename.cpp \
    source/main/main.cpp \

HEADERS += \
    include/common/FRCommon.h \
    include/common/FRFileInfo.h \
    include/common/FRLineEdit.h \
    include/common/FRRadioButton.h \
    include/module/FRInitMod.h \
    include/module/FRAddMod.h \
    include/module/FRRepMod.h \
    include/module/FRNumMod.h \
    include/module/FRLetMod.h \
    include/module/FRModule.h \
    include/module/FRModuleDef.h \
    include/module/FRModuleFactory.h \
    include/tool/FRDealFile.h \
    include/tool/FRHistory.h \
    include/tool/FRThreadPool.h \
    include/tool/FRAdjustThread.h \
    include/tool/FRDealPart.h \
    include/main/FileRename.h \

FORMS += \
    ui/FRAdjustThread.ui \
    ui/FileRename.ui \
    ui/FRDealPart.ui \

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource/image.qrc
