#include "FRAddMod.h"

int FRAddMod::Modify(FRModuleParam &moduleParam, QString &qstrSource)
{
    FRAddParam &addParam = dynamic_cast<FRAddParam &>(moduleParam);
    int ret = 0;

    switch (addParam.eAddMethod)
    {
    case FRAddMethod::FR_Method_Add_Head:       // 头部添加
        qstrSource.prepend(addParam.qstrAddHeadStr);
        break;
    case FRAddMethod::FR_Method_Add_Tail:       // 尾部添加
        qstrSource.append(addParam.qstrAddTailStr);
        break;
    case FRAddMethod::FR_Method_Add_SubFront:   // 子串前添加
        {
            FRCommon::StrIndex indexs = FRCommon::IndexOfStr(qstrSource, addParam.qstrAddSubFront, moduleParam.eCase);

            if (indexs.first >= 0)
            {
                qstrSource.replace(addParam.qstrAddSubFront, addParam.qstrAddSubFrontStr +
                                   qstrSource.mid(indexs.first, indexs.second - indexs.first + 1), moduleParam.eCase);
            }
        }
        break;
    case FRAddMethod::FR_Method_Add_SubBack:    // 字串后添加
        {
            FRCommon::StrIndex indexs = FRCommon::IndexOfStr(qstrSource, addParam.qstrAddSubBack, moduleParam.eCase);

            if (indexs.first >= 0)
            {
                qstrSource.replace(addParam.qstrAddSubBack, qstrSource.mid(indexs.first, indexs.second - indexs.first + 1) +
                                   addParam.qstrAddSubBackStr, moduleParam.eCase);
            }
        }
        break;
    case FRAddMethod::FR_Method_Add_SecFront:   // 区间前添加
        {
            FRCommon::StrIndex indexs = FRCommon::IndexOfStr(qstrSource, addParam.qstrAddFrontFront,
                                                   addParam.qstrAddFrontBack, moduleParam.eCase);

            if (indexs.first >= 0)
            {
                qstrSource.insert(indexs.first, addParam.qstrAddSecFrontStr);
            }
        }
        break;
    case FRAddMethod::FR_Method_Add_SecBack:    // 区间后添加
        {
            FRCommon::StrIndex indexs = FRCommon::IndexOfStr(qstrSource, addParam.qstrAddBackFront,
                                                   addParam.qstrAddBackBack, moduleParam.eCase);

            if (indexs.first >= 0)
            {
                qstrSource.insert(indexs.second + 1, addParam.qstrAddSecBackStr);
            }
        }
        break;
    default:
        ret = 1;
        break;
    }

    return ret;
}
