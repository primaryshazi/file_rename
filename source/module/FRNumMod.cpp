#include "FRNumMod.h"

int FRNumMod::Modify(FRModuleParam &moduleParam, QString &qstrSource)
{
    FRNumParam &numParam = dynamic_cast<FRNumParam &>(moduleParam);
    int ret = 0;

    QString qstrTemp = QString("%1").arg(numParam.iNumber, numParam.iNumFigure, 10, QChar('0'));

    switch (numParam.eNumMethod)
    {
    case FRNumMethod::FR_Method_Num_Head:       // 头部编号
        qstrSource.prepend(qstrTemp);
        numParam.iNumber += numParam.iNumSpan;
        break;
    case FRNumMethod::FR_Method_Num_Tail:       // 尾部编号
        qstrSource.append(qstrTemp);
        numParam.iNumber += numParam.iNumSpan;
        break;
    case FRNumMethod::FR_Method_Num_SubFront:   // 子串前编号
        {
            FRCommon::StrIndex indexs = FRCommon::IndexOfStr(qstrSource, numParam.qstrNumSubFront, numParam.eCase);

            if (indexs.first >= 0)
            {
                qstrSource.replace(numParam.qstrNumSubFront, qstrTemp +
                                   qstrSource.mid(indexs.first, indexs.second - indexs.first + 1), numParam.eCase);
                numParam.iNumber += numParam.iNumSpan;
            }
        }
        break;
    case FRNumMethod::FR_Method_Num_SubBack:    // 子串后编号
        {
            FRCommon::StrIndex indexs = FRCommon::IndexOfStr(qstrSource, numParam.qstrNumSubBack, numParam.eCase);

            if (indexs.first >= 0)
            {
                qstrSource.replace(numParam.qstrNumSubBack, qstrSource.mid(indexs.first, indexs.second - indexs.first + 1) +
                                   qstrTemp, numParam.eCase);
                numParam.iNumber += numParam.iNumSpan;
            }
        }
        break;
    case FRNumMethod::FR_Method_Num_SecFront:   // 区间前编号
        {
            FRCommon::StrIndex indexs = FRCommon::IndexOfStr(qstrSource, numParam.qstrNumFrontFront, numParam.qstrNumFrontBack,
                                                   numParam.eCase);

            if (indexs.first >= 0)
            {
                qstrSource.insert(indexs.first, qstrTemp);
                numParam.iNumber += numParam.iNumSpan;
            }
        }
        break;
    case FRNumMethod::FR_Method_Num_SecBack:    // 区间后编号
        {
            FRCommon::StrIndex indexs = FRCommon::IndexOfStr(qstrSource, numParam.qstrNumBackFront, numParam.qstrNumBackBack,
                                                   numParam.eCase);

            if (indexs.first >= 0)
            {
                qstrSource.insert(indexs.second + 1, qstrTemp);
                numParam.iNumber += numParam.iNumSpan;
            }
        }
        break;
    default:
        ret = 1;
        break;
    }

    return ret;
}
