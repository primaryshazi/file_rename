#include "FRRepMod.h"

int FRRepMod::Modify(FRModuleParam &moduleParam, QString &qstrSource)
{
    FRRepParam &repParam = dynamic_cast<FRRepParam &>(moduleParam);
    int ret = 0;

    switch (repParam.eRepMethod)
    {
    case FRRepMethod::FR_Method_Rep_Regex:      // 正则表达式替换
        qstrSource.replace(QRegExp(repParam.qstrRepRegex, repParam.eCase), repParam.qstrRepRegexStr);
        break;
    case FRRepMethod::FR_Method_Rep_Sub:        // 子串替换
        qstrSource.replace(repParam.qstrRepSub, repParam.qstrRepSubStr, repParam.eCase);
        break;
    case FRRepMethod::FR_Method_Rep_SubFront:   // 子串之前替换
        {
            FRCommon::StrIndex indexs = FRCommon::LastIndexOfStr(qstrSource, repParam.qstrRepSubFront, repParam.eCase);

            if (indexs.first > 0)
            {
                qstrSource.remove(0, indexs.first);
                qstrSource.prepend(repParam.qstrRepSubFrontStr);
            }
        }
        break;
    case FRRepMethod::FR_Method_Rep_SubBack:    // 子串之后替换
        {
            FRCommon::StrIndex indexs = FRCommon::IndexOfStr(qstrSource, repParam.qstrRepSubBack, repParam.eCase);

            if (indexs.first >= 0 && indexs.second < qstrSource.size() - 1)
            {
                qstrSource.remove(indexs.second + 1, qstrSource.size() - indexs.second - 1);
                qstrSource.append(repParam.qstrRepSubBackStr);
            }
        }
        break;
    case FRRepMethod::FR_Method_Rep_Sec:        // 区间替换
        {
            FRCommon::StrIndex indexs = FRCommon::IndexOfStr(qstrSource, repParam.qstrRepSecFront,
                                                       repParam.qstrRepSecBack, repParam.eCase);

            if (indexs.first >= 0)
            {
                qstrSource.replace(indexs.first, indexs.second - indexs.first + 1, repParam.qstrRepSecStr);
            }
        }
        break;
    default:
        ret = 1;
        break;
    };

    return ret;
}
