#include "FRModuleFactory.h"

std::shared_ptr<FRModule> FRModuleFactory::GetModule(FRModuleType eModule)
{
    MapModule::iterator it = m_mapModule.find(eModule);
    // 当模块不存在时添加方法对象
    if (m_mapModule.end() == it)
    {
        // 依据不同的方法调用不同的类
        switch (eModule)
        {
        case FRModuleType::FR_Module_Addition:
            it = m_mapModule.insert(std::make_pair(eModule, std::make_shared<FRAddMod>())).first;
            break;
        case FRModuleType::FR_Module_Replace:
            it = m_mapModule.insert(std::make_pair(eModule, std::make_shared<FRRepMod>())).first;
            break;
        case FRModuleType::FR_Module_Number:
            it = m_mapModule.insert(std::make_pair(eModule, std::make_shared<FRNumMod>())).first;
            break;
        case FRModuleType::FR_Module_Letter:
            it = m_mapModule.insert(std::make_pair(eModule, std::make_shared<FRLetMod>())).first;
            break;
        default:
            it = m_mapModule.insert(std::make_pair(eModule, std::make_shared<FRInitMod>())).first;
            break;
        }
    }

    return it->second;
}
