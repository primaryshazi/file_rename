#include "FRLetMod.h"

int FRLetMod::Modify(FRModuleParam &moduleParam, QString &qstrSource)
{
    FRLetParam &letParam = dynamic_cast<FRLetParam &>(moduleParam);
    int ret = 0;

    switch (letParam.eLetMethod)
    {
    case FRLetMethod::FR_Method_Let_Up:         // 全部大写
        qstrSource = qstrSource.toUpper();
        break;
    case FRLetMethod::FR_Method_Let_Low:        // 全部小写
        qstrSource = qstrSource.toLower();
        break;
    case FRLetMethod::FR_Method_Let_SubUp:      // 子串大写
        {
            FRCommon::StrIndex indexs = FRCommon::IndexOfStr(qstrSource, letParam.qstrLetSubUp, letParam.eCase);

            if (indexs.first >= 0)
            {
                QString qstrTemp = qstrSource.mid(indexs.first, indexs.second - indexs.first + 1);

                qstrSource.replace(qstrTemp, qstrTemp.toUpper(), letParam.eCase);
            }
        }
        break;
    case FRLetMethod::FR_Method_Let_SubLow:     // 子串小写
        {
            FRCommon::StrIndex indexs = FRCommon::IndexOfStr(qstrSource, letParam.qstrLetSubLow, letParam.eCase);

            if (indexs.first >= 0)
            {
                QString qstrTemp = qstrSource.mid(indexs.first, indexs.second - indexs.first + 1);

                qstrSource.replace(qstrTemp, qstrTemp.toLower(), letParam.eCase);
            }
        }
        break;
    case FRLetMethod::FR_Method_Let_SecUp:      // 区间大写
        {
            FRCommon::StrIndex indexs = FRCommon::IndexOfStr(qstrSource, letParam.qstrLetUpFront,
                                                   letParam.qstrLetUpBack, letParam.eCase);

            if (indexs.first >= 0)
            {
                std::for_each(qstrSource.begin() + indexs.first, qstrSource.begin() + indexs.second + 1,
                              [](QChar &qc) { qc = qc.toUpper(); });
            }
        }
        break;
    case FRLetMethod::FR_Method_Let_SecLow:     // 区间小写
        {
            FRCommon::StrIndex indexs = FRCommon::IndexOfStr(qstrSource, letParam.qstrLetLowFront,
                                                   letParam.qstrLetLowBack, letParam.eCase);

            if (indexs.first >= 0)
            {
                std::for_each(qstrSource.begin() + indexs.first, qstrSource.begin() + indexs.second + 1,
                              [](QChar &qc) { qc = qc.toLower(); });
            }
        }
        break;
    default:
        ret = 1;
        break;
    }

    return ret;
}
