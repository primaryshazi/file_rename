#include "FRDealFile.h"

void FRDealFile::DealRepeat()
{
    // 储存所有文件的临时路径
    QMap<QString, int> mapStrI;

    for (FRFileInfo &fi : m_listFileInfo)
    {
        // 不区分大小写
        QString tempPath = (fi.DirectoryRef() + FR_SLASH + fi.TempPrefixRef()).toUpper()
                + FR_POINT + fi.TempSuffixRef().toUpper();

        mapStrI[tempPath]++;

        int count = mapStrI[tempPath];

        // 当文件的临时路径出现重复时就会对重复的文件路径进行编号
        if (count > 1)
        {
            fi.TempPrefixRef() += QString("%1").arg(count);
        }
    }
}

FRDealFile &FRDealFile::GetInstance()
{
    static FRDealFile dealFile;

    return dealFile;
}

void FRDealFile::Insert(const QList<FRFileInfo> &c_listFI, const int c_index)
{
    if (c_index < 0)
    {
        m_listFileInfo.append(c_listFI);
    }
    else
    {
        // 避免添加多个时前者影响后者
        for (int i = 0; i < c_listFI.size(); i++)
        {
            m_listFileInfo.insert(c_index + i, c_listFI[i]);
        }
    }
}

void FRDealFile::Remove(const QList<int> &c_listI)
{
    // 避免删除多个时前者影响后者
    for (int i = 0; i < c_listI.size(); i++)
    {
        m_listFileInfo.removeAt(c_listI[i] - i);
    }
}

void FRDealFile::Clear()
{
    m_listFileInfo.clear();
}

void FRDealFile::Recover()
{
    std::for_each(m_listFileInfo.begin(), m_listFileInfo.end(), [&](FRFileInfo & si) {
        si.ModifyToReal();
    });
}

void FRDealFile::SetFileInfo(const QList<FRFileInfo> &c_listFI)
{
    m_listFileInfo = c_listFI;
    for (FRFileInfo &fi : m_listFileInfo)
    {
        // 临时文件名中存在不合法字符则直接不予处理
        if (!FRCommon::CheckVaildSymbol(fi.TempPrefixRef()))
        {
            fi.ModifyToReal();
            continue;
        }
        // 移除左端空白字符，右端空白和点字符
        FRCommon::LeftTrim(fi.TempPrefixRef());
        FRCommon::RightTrim(fi.TempPrefixRef(), " .");

        if (fi.TempPrefixRef().isEmpty() && fi.TempSuffixRef().isEmpty())
        {
            fi.TempPrefixRef() = FR_UNTITLED;
        }
    }
    DealRepeat();
}

QList<FRFileInfo> FRDealFile::GetFileInfo() const
{
    return m_listFileInfo;
}

FRFileInfo FRDealFile::GetFileInfo(const int c_index) const
{
    if(c_index < 0 || c_index > m_listFileInfo.size() - 1)
    {
        return FRFileInfo();
    }

    return m_listFileInfo[c_index];
}

int FRDealFile::Size() const
{
    return m_listFileInfo.size();
}

void FRDealFile::Swap(const int c_indexFirst, const int c_indexSecond)
{
    m_listFileInfo.swap(c_indexFirst, c_indexSecond);
}

void FRDealFile::Sort(const int c_column, bool isAscend)
{
    std::sort(m_listFileInfo.begin(), m_listFileInfo.end(), [&](const FRFileInfo &first, const FRFileInfo &second) -> bool {
        switch (c_column)
        {
        case FRColumnID::FR_Column_Real:
            if (isAscend)
            {
                return first.RealName().toUpper() < second.RealName().toUpper();
            }
            else
            {
                return first.RealName().toUpper() > second.RealName().toUpper();
            }
        case FRColumnID::FR_Column_Prefix:
            if (isAscend)
            {
                return first.TempPrefix().toUpper() < second.TempPrefix().toUpper();
            }
            else
            {
                return first.TempPrefix().toUpper() > second.TempPrefix().toUpper();
            }
        case FRColumnID::FR_Column_Suffix:
            if (isAscend)
            {
                return first.TempSuffix().toUpper() < second.TempSuffix().toUpper();
            }
            else
            {
                return first.TempSuffix().toUpper() > second.TempSuffix().toUpper();
            }
        case FRColumnID::FR_Column_Dir:
            if (isAscend)
            {
                return first.Directory().toUpper() < second.Directory().toUpper();
            }
            else
            {
                return first.Directory().toUpper() > second.Directory().toUpper();
            }
        default:
            if (isAscend)
            {
                return first < second;
            }
            else
            {
                return first > second;
            }
        }
    });
}

const FRFileInfo &FRDealFile::Index(int index) const
{
    return m_listFileInfo.at(index);
}
