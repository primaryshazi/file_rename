#include "FRDealPart.h"
#include "ui_FRDealPart.h"

FRDealPart::FRDealPart(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FRDealPart)
{
    ui->setupUi(this);

    this->setFixedSize(320, 80);
    ui->radioButtonDealPartPrefix->setChecked(true);
}

FRDealPart::~FRDealPart()
{
    delete ui;
}

void FRDealPart::on_radioButtonDealPartPrefix_clicked()
{
    this->close();
}

void FRDealPart::on_radioButtonDealPartSuffix_clicked()
{
    this->close();
}

FRDealPartMode FRDealPart::DealPartMode() const
{
    if (ui->radioButtonDealPartPrefix->isChecked())
    {
        return FRDealPartMode::FR_DealPart_Prefix;
    }
    else
    {
        return FRDealPartMode::FR_DealPart_Suffix;
    }
}
