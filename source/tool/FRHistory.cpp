#include "FRHistory.h"

FRHistory &FRHistory::GetInstance()
{
    static FRHistory history;

    return history;
}

void FRHistory::Clear()
{
    m_mapHistoryFI.clear();
    m_currentIndex = -1;
    m_size = 0;
}

int FRHistory::Size()
{
    return m_size;
}

QList<FRRecordFI> FRHistory::GetRecord() const
{
    QList<FRRecordFI> listRecordFI;

    for (MapHistoryFI::const_iterator c_it = m_mapHistoryFI.cbegin(); c_it != m_mapHistoryFI.cend(); c_it++)
    {
        // 当前名称，历史名称，文件路径
        FRRecordFI rfi(c_it.key().RealName(), c_it.value()[m_currentIndex].RealName(), c_it.key().Directory());

        listRecordFI.append(rfi);
    }

    return listRecordFI;
}

void FRHistory::SetRecord(const QList<FRRecordFI> &c_listRecordFI)
{
    // 临时历史记录
    MapHistoryFI tempMapHistoryFI;

    for (const FRRecordFI &c_rfi : c_listRecordFI)
    {
        // 此次名称
        FRFileInfo currentFI(c_rfi.directory + FR_SLASH + c_rfi.currentName);
        // 上次名称
        FRFileInfo lastFI(c_rfi.directory + FR_SLASH + c_rfi.lastName);
        // 单文件历史信息
        QList<FRFileInfo> listFI = m_mapHistoryFI[lastFI];

        // 追加此次文件信息
        listFI.append(currentFI);
        tempMapHistoryFI[currentFI] = listFI;
    }

    m_mapHistoryFI = std::move(tempMapHistoryFI);
    m_currentIndex = m_size;
    m_size++;
}

void FRHistory::InsertNewFI(const QList<FRFileInfo> &c_listFI)
{
    // 当初次记录时将记录数置为1
    if (0 == m_size)
    {
        m_currentIndex = 0;
        m_size = 1;
    }

    for (const FRFileInfo &c_fi : c_listFI)
    {
        // 历史记录不存在则添加
        if (m_mapHistoryFI.find(c_fi) == m_mapHistoryFI.end())
        {
            QList<FRFileInfo> listFI;

            // 补足之前存在的历史记录次数
            for (int i = 0; i < m_size; i++)
            {
                listFI.append(c_fi);
            }
            m_mapHistoryFI[c_fi] = std::move(listFI);
        }
    }
}

void FRHistory::RemoveOldFI(const QList<FRFileInfo> &c_listFI)
{
    for (const FRFileInfo &c_fi : c_listFI)
    {
        // 历史记录存在则删除
        if (m_mapHistoryFI.find(c_fi) != m_mapHistoryFI.end())
        {
            m_mapHistoryFI.remove(c_fi);
        }
    }
}

int FRHistory::CurrentIndex() const
{
    return m_currentIndex;
}

void FRHistory::IncreaseIndex()
{
    m_currentIndex++;
}

void FRHistory::ReduceIndex()
{
    m_currentIndex--;
}
