#include "FRAdjustThread.h"
#include "ui_FRAdjustThread.h"

FRAdjustThread::FRAdjustThread(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FRAdjustThread),
    m_iThreadSize(0)
{
    ui->setupUi(this);

    this->setFixedSize(360, 80);

    // 设置显示样式
    QPalette lcdPalette = ui->pushButton->palette();
    lcdPalette.setColor(QPalette::Normal, QPalette::WindowText, Qt::black);
    ui->lcdNumber->setPalette(lcdPalette);
    ui->lcdNumber->setSegmentStyle(QLCDNumber::Flat);
}

FRAdjustThread::~FRAdjustThread()
{
    delete ui;
}

void FRAdjustThread::UpdateLCD(const int c_value)
{
    if (0 != c_value)
    {
        m_iThreadSize = c_value;
    }
    ui->horizontalSlider->setValue(m_iThreadSize);
}

void FRAdjustThread::on_horizontalSlider_valueChanged(int value)
{
    ui->lcdNumber->display(QString("%1").arg(value, ui->lcdNumber->digitCount(), 10, QChar('0')));
}

void FRAdjustThread::on_pushButton_clicked()
{
    m_iThreadSize = static_cast<int>(ui->lcdNumber->value());
    emit sizeChange(m_iThreadSize);
    this->close();
}
