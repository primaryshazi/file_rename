#include "FRRadioButton.h"

FRRadioButton::FRRadioButton(QWidget *parent) : QRadioButton(parent)
{
}

FRRadioButton::~FRRadioButton()
{
}

int FRRadioButton::Method(const int c_method)
{
    if (c_method >= 0)
    {
        m_method = c_method;
    }

    return m_method;
}
