#include "FRFileInfo.h"

FRFileInfo::FRFileInfo(const QString &c_qstrPath)
{
    // 分离文件路径
    int index = c_qstrPath.lastIndexOf(FR_SLASH);

    if (index >= 0)
    {
        m_realName = c_qstrPath.mid(index + 1);
        m_directory = c_qstrPath.left(index);

        FilePreSuf file = SplitPreSuf();

        m_tempPrefix = file.first;
        m_tempSuffix = file.second;
    }
}

FRFileInfo::FRFileInfo(const FRFileInfo &c_fileInfo)
    : m_realName(c_fileInfo.m_realName)
    , m_tempPrefix(c_fileInfo.m_tempPrefix)
    , m_tempSuffix(c_fileInfo.m_tempSuffix)
    , m_directory(c_fileInfo.m_directory)
{
}

FRFileInfo::FRFileInfo(const QString &c_realName, const QString &c_tempPrefix,
                       const QString &c_tempSuffix, const QString &c_directory)
    : m_realName(c_realName)
    , m_tempPrefix(c_tempPrefix)
    , m_tempSuffix(c_tempSuffix)
    , m_directory(c_directory)
{
}

FRFileInfo &FRFileInfo::operator=(const FRFileInfo &c_fileInfo)
{
    if (*this != c_fileInfo)
    {
        m_realName = c_fileInfo.m_realName;
        m_tempPrefix = c_fileInfo.m_tempPrefix;
        m_tempSuffix = c_fileInfo.m_tempSuffix;
        m_directory = c_fileInfo.m_directory;
    }

    return *this;
}

QString &FRFileInfo::RealNameRef()
{
    return m_realName;
}

QString &FRFileInfo::TempPrefixRef()
{
    return m_tempPrefix;
}

QString &FRFileInfo::TempSuffixRef()
{
    return m_tempSuffix;
}

QString &FRFileInfo::DirectoryRef()
{
    return m_directory;
}

const QString &FRFileInfo::RealName() const
{
    return m_realName;
}

const QString &FRFileInfo::TempPrefix() const
{
    return m_tempPrefix;
}

const QString &FRFileInfo::TempSuffix() const
{
    return m_tempSuffix;
}

const QString &FRFileInfo::Directory() const
{
    return m_directory;
}

FilePreSuf FRFileInfo::SplitPreSuf()
{
    /**
     * c_qstrFile = "temp.tar.gz"
     *
     * file.first = "temp.tar"
     * file.second = ".gz"
     */

    FilePreSuf file;
    int index = m_realName.lastIndexOf('.');

    if (-1 == index)
    {
        // 无后缀则均为前缀
        file.first = m_realName;
    }
    else
    {
        // .之前为前缀
        file.first = m_realName.left(index);
        // .之后为后缀
        file.second = m_realName.mid(index + 1);
    }

    return file;
}

void FRFileInfo::ModifyToReal()
{
    FilePreSuf file = SplitPreSuf();

    m_tempPrefix = file.first;
    m_tempSuffix = file.second;
}

void FRFileInfo::ModifyToTemp()
{
    m_realName = m_tempPrefix + (m_tempSuffix.isEmpty() ? "" : FR_POINT + m_tempSuffix);
}

bool FRFileInfo::operator==(const FRFileInfo &c_fileInfo) const
{
    // 路径相等时此文件信息相同
    return this->m_directory == c_fileInfo.m_directory && this->m_realName == c_fileInfo.m_realName;
}

bool FRFileInfo::operator!=(const FRFileInfo &c_fileInfo) const
{
    // 路径相等时此文件信息相同
    return this->m_directory != c_fileInfo.m_directory || this->m_realName != c_fileInfo.m_realName;
}

bool FRFileInfo::operator>(const FRFileInfo &c_fileInfo) const
{
    QString thisFullPath = this->m_directory + FR_SLASH + this->m_realName;
    QString thatFullPath = c_fileInfo.m_directory + FR_SLASH + c_fileInfo.m_realName;

    // 按照文件全路径进行自然排序
    return thisFullPath > thatFullPath;
}

bool FRFileInfo::operator<(const FRFileInfo &c_fileInfo) const
{
    QString thisFullPath = this->m_directory + FR_SLASH + this->m_realName;
    QString thatFullPath = c_fileInfo.m_directory + FR_SLASH + c_fileInfo.m_realName;

    // 按照文件全路径进行自然排序
    return thisFullPath < thatFullPath;
}
