#include "FRCommon.h"

QList<QObject *> FRCommon::GetAllObjects(QObject *pObj)
{
    QList<QObject *> listObjs = { pObj };

    for (int i = 0; i < listObjs.size(); i++)
    {
        // 获取当前对象的子对象
        QObjectList objChildren = listObjs[i]->children();

        // 将所有子对象再插入到尾部
        listObjs.append(objChildren);
    }

    return listObjs;
}

void FRCommon::LeftTrim(QString &qstr, const QString &c_qstrTarget)
{
    while (!qstr.isEmpty())
    {
        // 若查找不到则退出循环
        if (c_qstrTarget.indexOf(qstr.front()) == -1)
        {
            break;
        }
        qstr.remove(0, 1);
    }
}

void FRCommon::RightTrim(QString &qstr, const QString &c_qstrTarget)
{
    while (!qstr.isEmpty())
    {
        // 若查找不到则退出循环
        if (c_qstrTarget.indexOf(qstr.back()) == -1)
        {
            break;
        }
        qstr.remove(qstr.length() - 1, 1);
    }
}

bool FRCommon::CheckVaildSymbol(const QString &c_qstr)
{
    const QString qstrVaild = R"([\\/:\*\?<>\|]+)";

    // 找不到匹配字符则为真
    if (c_qstr.indexOf(QRegExp(qstrVaild)) == -1)
    {
        return true;
    }

    return false;
}

FRCommon::StrIndex FRCommon::IndexOfStr(const QString &c_qstrSource, const QString &c_qstr, Qt::CaseSensitivity eCase)
{
    StrIndex indexs(-1, -1);

    if (!c_qstr.isEmpty())
    {
        int index = c_qstrSource.indexOf(c_qstr, 0, eCase);

        if (index >= 0)
        {
            indexs.first = index;
            indexs.second = index + c_qstr.size() - 1;
        }
    }

    return indexs;
}

FRCommon::StrIndex FRCommon::IndexOfStr(const QString &c_qstrSource, const QString &c_qstrFront, const QString &c_qstrBack,
                              Qt::CaseSensitivity eCase)
{
    StrIndex indexs(-1, -1);
    int indexBeg = -1;
    int indexEnd = -1;

    if (!c_qstrFront.isEmpty())
    {
        indexBeg = c_qstrSource.indexOf(c_qstrFront, 0, eCase);
    }

    if (!c_qstrBack.isEmpty())
    {
        indexEnd = c_qstrSource.lastIndexOf(c_qstrBack, -1, eCase);
    }

    // 当左区间小于等于右区间时则有效
    if (indexBeg >= 0 && indexEnd >= indexBeg)
    {
        indexs.first = indexBeg;
        indexs.second = indexEnd + c_qstrBack.size() - 1;
    }

    return indexs;
}

FRCommon::StrIndex FRCommon::LastIndexOfStr(const QString &c_qstrSource, const QString &c_qstr, Qt::CaseSensitivity eCase)
{
    StrIndex indexs(-1, -1);

    if (!c_qstr.isEmpty())
    {
        int index = c_qstrSource.lastIndexOf(c_qstr, -1, eCase);

        if (index >= 0)
        {
            indexs.first = index;
            indexs.second = index + c_qstr.size() - 1;
        }
    }

    return indexs;
}
