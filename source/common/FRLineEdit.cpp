#include "FRLineEdit.h"

FRLineEdit::FRLineEdit(QWidget *parent) : QLineEdit(parent)
{
}

FRLineEdit::~FRLineEdit()
{
}

void FRLineEdit::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::RightButton)
    {
        emit clicked();
    }

    QLineEdit::mousePressEvent(event);
}

int FRLineEdit::Method(const int c_method)
{
    if (c_method >= 0)
    {
        m_method = c_method;
    }

    return m_method;
}
