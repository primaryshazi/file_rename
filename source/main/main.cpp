#include "FileRename.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    try
    {
        FileRename w;
        w.show();

        app.exec();
    }
    catch (std::exception &e)
    {
        QMessageBox::critical(nullptr, "异常", e.what());
        app.exit(EXIT_FAILURE);
    }

    return 0;
}
