/**
 * 此类用于定义文件信息结构
 */

#ifndef FRFILEINFO_H
#define FRFILEINFO_H

#include <QString>
#include <QList>
#include <QMap>

#include <utility>

#define FR_UNTITLED "untitled"  // 未命令文件
#define FR_SLASH    "/"         // 路径分隔符
#define FR_POINT    "."         // 点

#define FR_COLUMNTYPE_REAL      1   // 真实名称类型
#define FR_COLUMNTYPE_PREFIX    2   // 临时前缀类型
#define FR_COLUMNTYPE_SUFFIX    4   // 临时后缀类型
#define FR_COLUMNTYPE_DIR       8   // 文件目录类型

// 文件名前缀后缀
typedef std::pair<QString, QString> FilePreSuf;

/**
 * @brief 列ID
 */
enum FRColumnID
{
    FR_Column_Real = 0,     // 真实名称
    FR_Column_Prefix = 1,   // 临时前缀
    FR_Column_Suffix = 2,   // 临时后缀
    FR_Column_Dir = 3       // 文件目录
};

class FRFileInfo
{
private:
    QString m_realName;
    QString m_tempPrefix;
    QString m_tempSuffix;
    QString m_directory;

public:
    FRFileInfo() {}
    ~FRFileInfo() {}

    FRFileInfo(const QString &c_qstrPath);
    FRFileInfo(const FRFileInfo &c_fileInfo);
    FRFileInfo(const QString &c_realName, const QString &c_tempPrefix,
               const QString &c_tempSuffix, const QString &c_directory);
    FRFileInfo &operator=(const FRFileInfo &c_fileInfo);

    /**
     * @brief 真实名称的引用
     * @return QString &
     */
    QString &RealNameRef();

    /**
     * @brief 临时前缀的引用
     * @return QString &
     */
    QString &TempPrefixRef();

    /**
     * @brief 临时后缀的引用
     * @return QString &
     */
    QString &TempSuffixRef();

    /**
     * @brief 所在目录的引用
     * @return QString &
     */
    QString &DirectoryRef();

    /**
     * @brief 真实名称
     * @return const QString &
     */
    const QString &RealName() const;

    /**
     * @brief 临时前缀
     * @return const QString &
     */
    const QString &TempPrefix() const;

    /**
     * @brief 临时后缀
     * @return const QString &
     */
    const QString &TempSuffix() const;

    /**
     * @brief 所在目录
     * @return const QString &
     */
    const QString &Directory() const;

    /**
     * @brief 分割文件前后缀
     * @return FilePreSuf
     */
    FilePreSuf SplitPreSuf();

    /**
     * @brief 皆更改为真实名称
     */
    void ModifyToReal();

    /**
     * @brief 皆更改为临时名称
     */
    void ModifyToTemp();

    bool operator==(const FRFileInfo &c_fileInfo) const;
    bool operator!=(const FRFileInfo &c_fileInfo) const;
    bool operator>(const FRFileInfo &c_fileInfo) const;
    bool operator<(const FRFileInfo &c_fileInfo) const;
};

#endif // FRFILEINFO_H
