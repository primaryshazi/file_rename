/**
 * 此类用于公用枚举宏定义或函数库
 */

#ifndef FRCOMMON_H
#define FRCOMMON_H

#include <QString>
#include <QObjectList>
#include <QMap>
#include <QList>

#include <algorithm>
#include <utility>

namespace FRCommon
{
    // 字符串的头尾索引
    typedef std::pair<int, int> StrIndex;

    /**
     * @brief 获取当前界面上所有的对象
     * @param pObj
     * @return QList<QObject *>
     */
    QList<QObject *> GetAllObjects(QObject *pObj);

    /**
     * @brief 移除字符串左端连续存在于c_qstrTarget中的字符
     * @param qstr
     * @param c_qstrTarget
     */
    void LeftTrim(QString &qstr, const QString &c_qstrTarget = " ");

    /**
     * @brief 移除字符串右端连续存在于c_qstrTarget中的字符
     * @param qstr
     * @param c_qstrTarget
     */
    void RightTrim(QString &qstr, const QString &c_qstrTarget = " ");

    /**
     * @brief 检查字符串中的字符是否有效
     * @param c_qstr
     * @return bool
     */
    bool CheckVaildSymbol(const QString &c_qstr);

    /**
     * @brief 从头匹配字符串，匹配到第一个则返回源字符串中的匹配的前后索引，未匹配则返回(-1, -1)
     * @param c_qstrSource
     * @param c_qstr
     * @param eCase
     * @return StrIndex
     */
    StrIndex IndexOfStr(const QString &c_qstrSource, const QString &c_qstr,
                        Qt::CaseSensitivity eCase = Qt::CaseSensitive);

    /**
     * @brief 匹配区间，匹配到则返回源字符串中的匹配部分的前后索引，未匹配则返回(-1, -1)
     * @param c_qstrSource
     * @param c_qstrFront
     * @param c_qstrBack
     * @param eCase
     * @return StrIndex
     */
    StrIndex IndexOfStr(const QString &c_qstrSource, const QString &c_qstrFront, const QString &c_qstrBack,
                        Qt::CaseSensitivity eCase = Qt::CaseSensitive);

    /**
     * @brief 从尾匹配字符串，匹配第一个到则返回源字符串中的匹配的前后索引，未匹配则返回(-1, -1)
     * @param c_qstrSource
     * @param c_qstr
     * @param eCase
     * @return StrIndex
     */
    StrIndex LastIndexOfStr(const QString &c_qstrSource, const QString &c_qstr,
                            Qt::CaseSensitivity eCase = Qt::CaseSensitive);

    /**
     * @brief 获取指定类型的控件，可以指定控件名称包含项
     * @param c_listSource
     * @param c_qstrName
     * @return QList<TargetType *>
     */
    template <typename SourceType, typename TargetType>
    QList<TargetType *> GetTargetObjs(const QList<SourceType *> &c_listSource, const QString &c_qstrName = "")
    {
        QList<TargetType *> listTargetObj;

        for (SourceType *pSource : c_listSource)
        {
            TargetType *pTargetObj = dynamic_cast<TargetType *>(pSource);

            // 当字符串不为空时须保证对象名称中包含c_qstrName
            if (nullptr != pTargetObj && (c_qstrName.isEmpty() || pTargetObj->objectName().indexOf(c_qstrName) != -1))
            {
                listTargetObj.push_back(pTargetObj);
            }
        }

        return listTargetObj;
    }
}

#endif // FRCOMMON_H
