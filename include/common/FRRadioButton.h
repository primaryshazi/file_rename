/**
 * 此类用于重写QRadioButton
 */

#ifndef FRRADIOBUTTON_H
#define FRRADIOBUTTON_H

#include <QObject>
#include <QRadioButton>
#include <QMouseEvent>

class FRRadioButton : public QRadioButton
{
    Q_OBJECT
private:
    int m_method;

public:
    explicit FRRadioButton(QWidget *parent = nullptr);
    virtual ~FRRadioButton();

    /**
     * @brief 设置并返回方法
     * @param c_method
     * @return int
     */
    int Method(const int c_method = -1);

signals:
    void alter();
};

#endif // FRRADIOBUTTON_H
