/**
 * 此类用于重写QLineEdit
 */

#ifndef FRLINEEDIT_H
#define FRLINEEDIT_H

#include <QObject>
#include <QLineEdit>
#include <QMouseEvent>
#include <QKeyEvent>

class FRLineEdit : public QLineEdit
{
    Q_OBJECT
private:
    int m_method;

public:
    explicit FRLineEdit(QWidget *parent = nullptr);
    virtual ~FRLineEdit();

    virtual void mousePressEvent(QMouseEvent *event) override;

    /**
     * @brief 设置并返回方法
     * @param c_method
     * @return int
     */
    int Method(const int c_method = -1);

signals:
    void clicked();
};

#endif // FRLINEEDIT_H
