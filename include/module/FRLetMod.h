/**
 * 字母模块
 */

#ifndef FRLETMOD_H
#define FRLETMOD_H

#include "FRModuleDef.h"
#include "FRFileInfo.h"
#include "FRCommon.h"
#include "FRModule.h"

#include <QString>

#include <algorithm>

class FRLetMod : public FRModule
{
public:
    FRLetMod() {}
    virtual ~FRLetMod() {}

    /**
     * @brief 字母方法修改
     * @param moduleParam
     * @param qstrSource
     * @return int
     */
    virtual int Modify(FRModuleParam &moduleParam, QString &qstrSource) override;
};

#endif // FRLETMOD_H
