/**
 * 模块
 */

#ifndef FRMODULE_H
#define FRMODULE_H

#include "FRModuleDef.h"

class FRModule
{
public:
    FRModule() {}
    virtual ~FRModule() {}

    /**
     * @brief 指定方法修改源字符串
     * @param moduleParam
     * @param qstrSource
     * @return int
     */
    virtual int Modify(FRModuleParam &moduleParam, QString &qstrSource) = 0;
};

#endif // FRMODULE_H
