/**
 * 模块工厂
 */

#ifndef FRMODULEFACTROY_H_
#define FRMODULEFACTROY_H_

#include "FRModule.h"
#include "FRInitMod.h"
#include "FRAddMod.h"
#include "FRRepMod.h"
#include "FRNumMod.h"
#include "FRLetMod.h"

#include <memory>
#include <map>

class FRModuleFactory
{
private:
    typedef std::map<FRModuleType, std::shared_ptr<FRModule>> MapModule;
    typedef std::pair<MapModule::iterator, bool> InsertResult;

    MapModule m_mapModule;  // 储存对象方法

public:
    /**
     * @brief 获取模块对象
     * @param eModule
     * @return std::shared_ptr<FRModifyFile>
     */
    std::shared_ptr<FRModule> GetModule(FRModuleType eModule);
};

#endif // FRMODULEFACTROY_H_
