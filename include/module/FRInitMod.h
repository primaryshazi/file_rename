#ifndef FRINITMOD_H_
#define FRINITMOD_H_

#include "FRModuleDef.h"
#include "FRModule.h"

class FRInitMod : public FRModule
{
public:
    FRInitMod() {}
    virtual ~FRInitMod() {}

    /**
     * @brief 无方法
     * @return int
     */
    virtual int Modify(FRModuleParam &, QString &) override;
};

#endif // FRINITMOD_H_
