/**
 * 替换模块
 */

#ifndef FRREPMOD_H
#define FRREPMOD_H

#include "FRModuleDef.h"
#include "FRFileInfo.h"
#include "FRCommon.h"
#include "FRModule.h"

#include <QString>
#include <QRegExp>

class FRRepMod : public FRModule
{
public:
    FRRepMod() {}
    virtual ~FRRepMod() {}

    /**
     * @brief 替换方法修改
     * @param moduleParam
     * @param qstrSource
     * @return int
     */
    virtual int Modify(FRModuleParam &moduleParam, QString &qstrSource) override;
};

#endif // FRREPMOD_H
