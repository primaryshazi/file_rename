/**
 * 编号模块
 */

#ifndef FRNUMMOD_H
#define FRNUMMOD_H

#include "FRModuleDef.h"
#include "FRFileInfo.h"
#include "FRCommon.h"
#include "FRModule.h"

#include <QString>

class FRNumMod : public FRModule
{
public:
    FRNumMod() {}
    virtual ~FRNumMod() {}

    /**
     * @brief 编号方法修改
     * @param moduleParam
     * @param qstrSource
     * @return int
     */
    virtual int Modify(FRModuleParam &moduleParam, QString &qstrSource) override;
};

#endif // FRNUMMOD_H
