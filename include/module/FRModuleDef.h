/**
 * 模块定义
 */

#ifndef FRMODULEDEF_H_
#define FRMODULEDEF_H_

#include <QString>

/**
 * @brief 模块枚举
 */
enum FRModuleType
{
    FR_Module_Init = 0,
    FR_Module_Addition = 1,     // 添加模块
    FR_Module_Replace = 2,      // 替换模块
    FR_Module_Number = 3,       // 编号模块
    FR_Module_Letter = 4        // 字母模块
};

/**
 * @brief 添加模块方法枚举
 */
enum FRAddMethod
{
    FR_Method_Add_Init = 0,
    FR_Method_Add_Head = 1,         // 头部添加方法
    FR_Method_Add_Tail = 2,         // 尾部添加方法
    FR_Method_Add_SubFront = 3,     // 子串之前添加方法
    FR_Method_Add_SubBack = 4,      // 字串之后添加方法
    FR_Method_Add_SecFront = 5,     // 区间之前添加方法
    FR_Method_Add_SecBack = 6       // 区间之后添加方法
};

/**
 * @brief 替换模块方法枚举
 */
enum FRRepMethod
{
    FR_Method_Rep_Init = 0,
    FR_Method_Rep_Regex = 1,        // 正则替换方法
    FR_Method_Rep_Sub = 2,          // 子串替换方法
    FR_Method_Rep_SubFront = 3,     // 子串之前替换方法
    FR_Method_Rep_SubBack = 4,      // 子串之后替换方法
    FR_Method_Rep_Sec = 5           // 区间替换方法
};

/**
 * @brief 编号模块方法枚举
 */
enum FRNumMethod
{
    FR_Method_Num_Init = 0,
    FR_Method_Num_Head = 1,         // 头部编号方法
    FR_Method_Num_Tail = 2,         // 尾部编号方法
    FR_Method_Num_SubFront = 3,     // 子串之前编号方法
    FR_Method_Num_SubBack = 4,      // 子串之后编号方法
    FR_Method_Num_SecFront = 5,     // 区间之前编号方法
    FR_Method_Num_SecBack = 6       // 区间之后编号方法
};

/**
 * @brief 字母模块方法枚举
 */
enum FRLetMethod
{
    FR_Method_Let_Init = 0,
    FR_Method_Let_Up = 1,       // 字母全部大写方法
    FR_Method_Let_Low = 2,      // 字母全部小写方法
    FR_Method_Let_SubUp = 3,    // 字母子串大写方法
    FR_Method_Let_SubLow = 4,   // 字母子串小写方法
    FR_Method_Let_SecUp = 5,    // 字母区间大写方法
    FR_Method_Let_SecLow = 6    // 字母区间小写方法
};



/**
 * @brief 模块参数
 */
struct FRModuleParam
{
    FRModuleType eModule;               // 模块
    FRAddMethod eAddMethod;         // 添加方法
    FRRepMethod eRepMethod;         // 替换方法
    FRNumMethod eNumMethod;         // 编号方法
    FRLetMethod eLetMethod;         // 字母方法
    Qt::CaseSensitivity eCase;      // 是否区分大小写

    FRModuleParam()
        : eModule(FR_Module_Init)
        , eAddMethod(FRAddMethod::FR_Method_Add_Init)
        , eRepMethod(FRRepMethod::FR_Method_Rep_Init)
        , eNumMethod(FRNumMethod::FR_Method_Num_Init)
        , eLetMethod(FRLetMethod::FR_Method_Let_Init)
        , eCase(Qt::CaseSensitive) {}
    virtual ~FRModuleParam() {}
};

/**
 * @brief 添加模块参数
 */
struct FRAddParam : public FRModuleParam
{
    QString qstrAddHeadStr;         // 头部添加
    QString qstrAddTailStr;         // 尾部添加
    QString qstrAddSubFront;        // 子串前添加
    QString qstrAddSubFrontStr;
    QString qstrAddSubBack;         // 子串后添加
    QString qstrAddSubBackStr;
    QString qstrAddFrontFront;      // 区间前添加
    QString qstrAddFrontBack;
    QString qstrAddSecFrontStr;
    QString qstrAddBackFront;       // 区间后添加
    QString qstrAddBackBack;
    QString qstrAddSecBackStr;

    FRAddParam() {}
    virtual ~FRAddParam() {}
};

/**
 * @brief 替换模块参数
 */
struct FRRepParam : public FRModuleParam
{
    QString qstrRepRegex;           // 正则表达式替换
    QString qstrRepRegexStr;
    QString qstrRepSub;             // 子串替换
    QString qstrRepSubStr;
    QString qstrRepSubFront;        // 子串前添加
    QString qstrRepSubFrontStr;
    QString qstrRepSubBack;         // 子串后添加
    QString qstrRepSubBackStr;
    QString qstrRepSecFront;        // 区间替换
    QString qstrRepSecBack;
    QString qstrRepSecStr;

    FRRepParam() {}
    virtual ~FRRepParam() {}
};

/**
 * @brief 编号模块参数
 */
struct FRNumParam : public FRModuleParam
{
    int iNumStart;                  // 起始
    int iNumSpan;                   // 间隔
    int iNumFigure;                 // 位数
    int iNumber;                    // 序号
    QString qstrNumSubFront;        // 子串之前编号
    QString qstrNumSubBack;         // 子串之后编号
    QString qstrNumFrontFront;      // 区间之前编号
    QString qstrNumFrontBack;
    QString qstrNumBackFront;       // 区间之后编号
    QString qstrNumBackBack;

    FRNumParam() {}
    virtual ~FRNumParam() {}
};

/**
 * @brief 字母模块参数
 */
struct FRLetParam : public FRModuleParam
{
    QString qstrLetSubUp;           // 子串大写
    QString qstrLetSubLow;          // 子串小写
    QString qstrLetUpFront;         // 区间大写
    QString qstrLetUpBack;
    QString qstrLetLowFront;        // 区间小写
    QString qstrLetLowBack;

    FRLetParam() {}
    virtual ~FRLetParam() {}
};

#endif // FRMODULEDEF_H_
