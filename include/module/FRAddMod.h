/**
 * 添加模块
 */

#ifndef FRADDMOD_H
#define FRADDMOD_H

#include "FRModuleDef.h"
#include "FRFileInfo.h"
#include "FRCommon.h"
#include "FRModule.h"

#include <QString>

class FRAddMod : public FRModule
{
public:
    FRAddMod() {}
    virtual ~FRAddMod() {}

    /**
     * @brief 添加方法修改
     * @param moduleParam
     * @param qstrSource
     * @return int
     */
    virtual int Modify(FRModuleParam &moduleParam, QString &qstrSource) override;
};

#endif // FRADDMOD_H
