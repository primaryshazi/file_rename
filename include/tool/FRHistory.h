/**
 * 此类用于完成历史信息功能
 */

#ifndef FRHISTORY_H
#define FRHISTORY_H

#include "FRCommon.h"
#include "FRDealFile.h"
#include "FRFileInfo.h"

#include <memory>

/**
 * @brief 当前名称和历史名称
 */
typedef QMap<FRFileInfo, QList<FRFileInfo>> MapHistoryFI;

/**
 * @brief 文件信息修改前后记录
 */
struct FRRecordFI
{
    QString currentName;
    QString lastName;
    QString directory;

    FRRecordFI () {}
    FRRecordFI(const QString &c_currentName, const QString &c_lastName, const QString &c_dirctory) :
        currentName(c_currentName), lastName(c_lastName), directory(c_dirctory) {}
};

class FRHistory
{
private:
    /**
     * @brief 储存历史文件信息；
     * QList<FRFileInfo> : 记录一个文件的所有历史信息，顺序索引；
     * QMap<FRFileInfo, QList<FRFileInfo>> : 记录所有文件的历史信息；
     */
    MapHistoryFI m_mapHistoryFI;

    int m_currentIndex;     // 储存在历史信息中的索引
    int m_size;     // 历史记录的次数

private:
    FRHistory() : m_currentIndex(-1), m_size(0) {}

public:
    /**
     * @brief 获取历史储存实例
     * @return FRHistory &
     */
    static FRHistory &GetInstance();

    /**
     * @brief 清空历史记录
     */
    void Clear();

    /**
     * @brief 获取储存历史信息的数量
     * @return int
     */
    int Size();

    /**
     * @brief 获取当前索引处的所有文件信息
     * @return GetCurrentFI
     */
    QList<FRRecordFI> GetRecord() const;

    /**
     * @brief 记录当前历史文件信息
     * @param c_listFIRecord
     */
    void SetRecord(const QList<FRRecordFI> &c_listFIRecord);

    /**
     * @brief 插入未有的文件信息的历史记录
     * @param c_listFI
     */
    void InsertNewFI(const QList<FRFileInfo> &c_listFI);

    /**
     * @brief 删除原有的文件信息的历史记录
     * @param c_listFI
     */
    void RemoveOldFI(const QList<FRFileInfo> &c_listFI);

    /**
     * @brief 获取当前索引
     * @return int
     */
    int CurrentIndex() const;

    /**
     * @brief 增加索引
     */
    void IncreaseIndex();

    /**
     * @brief 减少索引
     */
    void ReduceIndex();
};

#endif // FRHISTORY_H
