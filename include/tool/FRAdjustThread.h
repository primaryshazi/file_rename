/**
 * 此类用于指定线程调节功能
 */

#ifndef FRADJUSTTHREAD_H
#define FRADJUSTTHREAD_H

#include <QMainWindow>
#include <QPushButton>
#include <QSlider>
#include <QLCDNumber>

namespace Ui {
class FRAdjustThread;
}

class FRAdjustThread : public QMainWindow
{
    Q_OBJECT

private:
    Ui::FRAdjustThread *ui;

    int m_iThreadSize;

public:
    explicit FRAdjustThread(QWidget *parent = nullptr);
    ~FRAdjustThread();

    /**
     * @brief 更新LCD显示
     * @param c_value
     */
    void UpdateLCD(const int c_value = 0);

signals:
    void sizeChange(int);

private slots:
    /**
     * @brief 将实时值显示在LCD上
     * @param value
     */
    void on_horizontalSlider_valueChanged(int value);

    /**
     * @brief 点击确认更改
     */
    void on_pushButton_clicked();
};

#endif // FRADJUSTTHREAD_H
