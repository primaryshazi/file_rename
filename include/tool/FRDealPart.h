#ifndef FRDEALPART_H
#define FRDEALPART_H

#include <QMainWindow>

/**
 * @brief 处理前后缀类型
 */
enum FRDealPartMode
{
    FR_DealPart_Prefix = 0,
    FR_DealPart_Suffix = 1
};

namespace Ui {
class FRDealPart;
}

class FRDealPart : public QMainWindow
{
    Q_OBJECT

private:
    Ui::FRDealPart *ui;

public:
    explicit FRDealPart(QWidget *parent = nullptr);
    ~FRDealPart();

private slots:
    /**
     * @brief 前缀模式
     */
    void on_radioButtonDealPartPrefix_clicked();

    /**
     * @brief 后缀模式
     */
    void on_radioButtonDealPartSuffix_clicked();

public:
    /**
     * @brief 获取处理模式
     * @return FRDealPartMode
     */
    FRDealPartMode DealPartMode() const;
};

#endif // FRDEALPART_H
