/**
 * 此类用于储存并处理文件名称信息
 */

#ifndef FRDEALFILE_H
#define FRDEALFILE_H

#include "FRCommon.h"
#include "FRFileInfo.h"

#include <QList>
#include <QMap>
#include <QString>

class FRDealFile
{
private:
    /**
     * @brief 储存文件的信息，按插入顺序
     */
    QList<FRFileInfo> m_listFileInfo;

private:
    FRDealFile() {}

    /**
     * @brief 处理临时名称重复
     */
    void DealRepeat();

public:
    /**
     * @brief 获取文件信息处理实例
     * @return FRDealFile &
     */
    static FRDealFile &GetInstance();

    /**
     * @brief 添加文件全路径
     * @param c_listFI
     * @param c_index
     */
    void Insert(const QList<FRFileInfo> &c_listFI, const int c_index = -1);

    /**
     * @brief 移除多个文件
     * @param c_listIndex
     */
    void Remove(const QList<int> &c_listIndex);

    /**
     * @brief 清空文件
     */
    void Clear();

    /**
     * @brief 恢复所有文件信息
     */
    void Recover();

    /**
     * @brief 设置所有文件信息
     * @param c_listFI
     */
    void SetFileInfo(const QList<FRFileInfo> &c_listFI);

    /**
     * @brief 获取所有文件信息
     * @return QList<FRFileInfo>
     */
    QList<FRFileInfo> GetFileInfo() const;

    /**
     * @brief 获取指定索引处的文件信息
     * @param index
     * @return
     */
    FRFileInfo GetFileInfo(const int c_index) const;

    /**
     * @brief 获取文件数量
     * @return int
     */
    int Size() const;

    /**
     * @brief 交换两个数据
     * @param c_indexFirst
     * @param c_indexSecond
     */
    void Swap(const int c_indexFirst, const int c_indexSecond);

    /**
     * @brief 按列排序
     * @param c_column
     * @param isAscend
     */
    void Sort(const int c_column, bool isAscend);

    /**
     * @brief 返回指定索引文件信息
     * @param index
     * @return const FRFileInfo &
     */
    const FRFileInfo &Index(int index) const;
};

#endif // FRDEALFILE_H
